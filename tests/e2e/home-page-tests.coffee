require('chai').should()

describe 'Home page test', ->

  it 'home page should say welcome to title', (done) ->
#    this.timeout 20000 # to increase mocha timeout
    browser.get browser.params.baseUrl
    welcomeElement = element(By.id('title'))

    welcomeElement.getText()
    .then (text) ->
      text.should.be.equal("Welcome to Home")
      done()
    return
