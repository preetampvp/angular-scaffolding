module.exports = function(config) {
  config.set({

    basePath: '..',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['browserify', 'mocha'],

    // list of files to exclude
    exclude: [],

    browserify: {
      watch: true,
      debug: true,
      transform: [ 'coffeeify' ],
      extensions: ['.coffee']
    },

    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
        'app/app.coffee' : ['browserify'],
        'tests/unit-tests/**/*.coffee': ['browserify']
    },

    files: [
      'node_modules/mocha/mocha.js',

      'app/app.coffee',
      {
        // watch application files, but do not serve them from Karma since they are served by browserify
        pattern: 'app/**/*.+(coffee|js)',
        watched: true,
        included: false,
        served: false
      },
      'tests/unit-tests/**/*-tests.+(coffee|js)'
    ],

    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};