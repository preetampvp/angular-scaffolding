'use strict'

require('angular-mocks/angular-mocks')
require('chai').should()

describe 'Home controller test', () ->

  controller = null
  scope = null

  beforeEach angular.mock.module('app.controllers')

  beforeEach angular.mock.inject ($rootScope, $controller) ->
    scope = $rootScope.$new()
    controller = $controller('Home', {})
    return

  it 'vm.Title to equal to home', () ->
    controller.Title.should.equal("Home")
    return

