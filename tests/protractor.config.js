exports.config = {

    // location of the Selenium JAR file and chromedriver, use these if you installed protractor locally
    seleniumServerJar: '../node_modules/protractor/selenium/selenium-server-standalone-2.44.0.jar',
    //seleniumArgs: ['-browserTimeout=60'],
    chromeDriver: '../node_modules/protractor/selenium/chromedriver',

    // location of your E2E test specs
    //specs: [
    //    'e2e/**/*.js'
    //],

    // configure multiple browsers to run tests
    multiCapabilities: [ {
        'browserName': 'chrome'
    }],

    getPageTimeout: 6000,
    // url where your app is running, relative URLs are prepending with this URL
    //baseUrl: 'http://localhost:9000/',

    params: {
      baseUrl: 'http://localhost:9000'
    },
    framework: 'mocha'
};