'use strict'

require('angular')
require('angular-ui-router')
require('angular-sanitize')
require('./core/module-controllers').setup()
routing = require('./core/routing')

angular
.module 'app', ['ngSanitize', 'ui.router', 'app.controllers']
.config ['$stateProvider', '$urlRouterProvider', routing]

# include templatecache
require('./templates')
