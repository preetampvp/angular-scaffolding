# manage application routing here.

routing = ($stateProvider, $urlRouterProvider) ->

  # default route
  $urlRouterProvider.otherwise 'home'

  stateProvider = $stateProvider
  stateProvider
  .state 'home', {
    url: '/home'
    views: {
      main: {
        templateUrl: 'app/home/home.html',
        controller: 'Home',
        controllerAs: 'vm'
      }
    }
  }

  return

module.exports = routing