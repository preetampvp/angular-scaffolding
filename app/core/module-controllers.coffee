'use strict'

require('angular')
Home = require('../home/home-controller')

module.exports = {
  setup: () ->
    angular.module('app.controllers', [])

    # register controllers here
    angular.module('app.controllers')
    .controller('Home', [Home])
}
