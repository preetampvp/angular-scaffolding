'use strict'

module.exports = function() {

    var app = './app/';
    var tmp = './.tmp/';
    var build = './.build/'

    var config = {
        coffeeGlob: app + '**/*.coffee',
        htmlTemplates: [app + '**/*.html', '!' + app + 'index.html'],
        tmp: tmp,
        tmpIndex: tmp + 'index.html',
        watch: app + '**/*.*',
        tmpJs: tmp + 'js/',
        build: build,
        buildJs: build + 'js/',
        dist: './.dist/',
        buildIndex: build + 'index.html',
        index: './app/index.html',
        bower: {
            json: require('./bower.json'),
            directory: './bower_components',
            ignorePath: '../..'
        },
        templateCache: {
            file: 'templates.js',
            options: {
                module: "app",
                standAlone: false,
                root: 'app/'
            }
        },
        karmaConfigFile: './tests/karma.config.js',
        protractorConfigFile: './tests/protractor.config.js',
        e2eTestsGlob: [ './tests/e2e/**/*.coffee' ]
    };

    config.getWiredepDefaultOptions = function() {
        var options = {
            bowerJson: config.bower.json,
            directory: config.bower.directory,
            ignorePath: config.bower.ignorePath
        };
        return options;
    };
    return config;
}