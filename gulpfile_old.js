var gulp        = require('gulp');
var $           = require('gulp-load-plugins')({ lazy:true });
var source      = require('vinyl-source-stream');
var buffer      = require('vinyl-buffer');
var browserify  = require('browserify');
var config      = require('./gulp.config.js')();
var del         = require('del');
var runSequence = require('run-sequence');
var connect     = require('gulp-connect');


gulp.task('coffeelint', function() {
    log('*** Running Coffeelint ****');
    return gulp.src(config.coffeeGlob)
            .pipe($.coffeelint())
            .pipe($.coffeelint.reporter('fail'));
});

// wiredep - automatically pull in bower dependencies into index.html based on bower.json
gulp.task('wiredep', function() {
    var wiredep = require('wiredep').stream;
    var options = config.getWiredepDefaultOptions();

    return gulp.src(config.index)
            .pipe(wiredep(options))
            .pipe(gulp.dest(config.build));
});

gulp.task('compilecoffee', function() {
    log('Converting .coffee --> .js');

    return gulp.src(config.coffeeGlob)
            .pipe($.coffee({bare:true}).on('error', $.util.log))
            .pipe(gulp.dest(config.tmpJs));
});

gulp.task('templatecache', function() {
    log('Creating AngularJS $templateCache');

    return gulp
        .src(config.htmlTemplates)
        .pipe($.minifyHtml({empty: true}))
        .pipe($.angularTemplatecache(
            config.templateCache.file,
            config.templateCache.options
        ))
        .pipe(gulp.dest(config.tmpJs));
});

gulp.task('browserify', function() {

    log("Bundling using browserify");

    var bundler = browserify({
        entries: [config.tmpJs + 'app.js'],
        insertGlobals: true
    });

    var bundle = function() {
        return bundler
            .bundle()
            .pipe(source('bundle.js'))
            .pipe(buffer())
            .pipe($.uglify())
            .pipe(gulp.dest(config.buildJs));
    };

    return bundle();
});

gulp.task('build', function(done) {
    runSequence(['coffeelint', 'clean-tmp', 'clean-build'], 'wiredep', 'compilecoffee', 'templatecache','browserify', done);
});

gulp.task('connect', function() {
    log('Starting server at port 9000');

    connect.server({
        root: ['./.build'],
        port: 9000,
        livereload: true
    });
});

gulp.task('open', function() {
    log('Opening localhost:9000 in browser');
    var options = {
        url: 'http://localhost:9000'
    };

    return gulp.src(config.buildIndex)
           .pipe($.open('', options));
});

gulp.task('reload', function() {
    return gulp.src(config.build + '**/*.*')
           .pipe(connect.reload());
});

gulp.task('reconnect', function(done) {
    runSequence('build', 'reload', done);
});

gulp.task('serve', function(done) {
    runSequence('build', 'connect', 'open', 'watch', done);
});

gulp.task('watch', function() {
    gulp.watch(config.watch, ['reconnect']);
});

gulp.task('clean-tmp', function(done) {
    clean(config.tmp, done);
});

gulp.task('clean-build', function(done) {
    clean(config.build, done);
});

gulp.task('help', function() {
    $.taskListing()
});

// todo list

// build to run unit tests

// dist for building deployment package and running e2e tests

// convert less to css and minify

// run unit tests

// run e2e

// versioning


////////////////////////////////////////////////////

function clean(path, done) {
    log('Cleaning: ' + $.util.colors.green(path));
    del(path, done);
}

function log(msg) {
    if (typeof(msg) === 'object') {
        for (var item in msg) {
            if (msg.hasOwnProperty(item)) {
                $.util.log($.util.colors.blue(msg[item]));
            }
        }
    } else {
        $.util.log($.util.colors.blue(msg));
    }
}


//gulp.task('appCSS', function() {
//  // concatenate compiled Less and CSS
//  // into build/app.css
//  gulp
//    .src([
//      './app/**/*.less',
//      './app/**/*.css'
//    ])
//    .pipe(
//      gulpif(/[.]less$/,
//        less({
//          paths: [
//            './bower_components/bootstrap/less'
//          ]
//        })
//        .on('error', gutil.log))
//    )
//    .pipe(
//      concat('app.css')
//    )
//    .pipe(
//      gulp.dest('./build')
//    )
//});
//

//gulp.task('libCSS',
//  function() {
//  // concatenate vendor css into build/lib.css
//  gulp.src(['!./bower_components/**/*.min.css',
//      './bower_components/**/*.css'])
//      .pipe(concat('lib.css'))
//      .pipe(gulp.dest('./build'));
//});
//
//gulp.task('index', function() {
//  gulp.src(['./app/index.jade', './app/index.html'])
//    .pipe(gulpif(/[.]jade$/, jade().on('error', gutil.log)))
//    .pipe(gulp.dest('./build'));
//});
//