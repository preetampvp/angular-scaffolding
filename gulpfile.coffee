gulp = require('gulp')
$ = require('gulp-load-plugins')({ lazy:true })
args = require('yargs').argv;
source = require('vinyl-source-stream')
buffer = require('vinyl-buffer')
browserify = require('browserify')
config = require('./gulp.config.js')()
del = require('del')
runSequence = require('run-sequence')
connect = require('gulp-connect')
karma = require('karma')
karmaParseConfig = require('karma/lib/config').parseConfig
path = require('path')


gulp.task 'coffeelint', () ->
  log '**** Running Coffeelint ****'
  gulp.src(config.coffeeGlob)
  .pipe($.coffeelint())
  .pipe($.coffeelint.reporter('fail'))

# wiredep - automatically pull in bower dependencies into index.html based on bower.json
gulp.task 'wiredep', () ->
  wiredep = require('wiredep').stream
  options = config.getWiredepDefaultOptions()

  gulp.src(config.index)
  .pipe(wiredep(options))
  .pipe(gulp.dest(config.tmp))

gulp.task 'compilecoffee', () ->
  log 'Converting .coffee --> .js'

  gulp.src(config.coffeeGlob)
  .pipe($.coffee({bare: true}).on('error', $.util.log))
  .pipe(gulp.dest(config.tmpJs))

gulp.task 'templatecache', () ->
  log 'Creating AngularJS $templateCache'

  gulp.src(config.htmlTemplates)
  .pipe($.minifyHtml({empty: true}))
  .pipe($.angularTemplatecache(config.templateCache.file, config.templateCache.options))
  .pipe(gulp.dest(config.tmpJs))

gulp.task 'browserify', () ->
  log 'Bundling using browserify'

  bundler = browserify({
    entries: [config.tmpJs + 'app.js'],
    insertGlobals: true
  })

  bundle = () ->
    bundler
    .bundle()
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe($.if(!args.debug, $.uglify()))
    .pipe(gulp.dest(config.buildJs))

  bundle()

gulp.task 'rev', () ->
  gulp.src("#{config.buildJs}**/*.js")
  .pipe($.rev())
  .pipe(gulp.dest(config.buildJs))
  .pipe($.rev.manifest())
  .pipe(gulp.dest(config.buildJs))

gulp.task 'revreplace' , () ->
  manifest = require("./.build/js/rev-manifest.json")
  stream = gulp.src(config.tmpIndex)

  Object.keys(manifest).reduce((stream, key) ->
    stream.pipe($.replace(key, manifest[key]))
  , stream).pipe(gulp.dest(config.build))

gulp.task 'build', (done) ->
  runSequence(['coffeelint', 'clean-tmp', 'clean-build'], 'wiredep', 'compilecoffee', 'templatecache','browserify', 'rev', 'revreplace', 'postbuild-cleanup', done)
  return

gulp.task 'connect', () ->
  log 'Starting server at port 9000'

  connect.server({
    root: ['./.build'],
    port: 9000,
    livereload: true
  })
  return

gulp.task 'stopConnect', (done) ->
  connect.serverClose()
  done()
  return

gulp.task 'open', () ->
  log 'Opening localhost:9000 in browser'
  options = {
    url: 'http://localhost:9000'
  }

  gulp.src(config.buildIndex)
  .pipe($.open('', options))

gulp.task 'reload', () ->
  gulp.src("#{config.build}**/*.*")
  .pipe(connect.reload())

gulp.task 'reconnect', (done) ->
  runSequence 'build', 'reload', done
  return

gulp.task 'serve', (done) ->
  runSequence 'build', 'connect', 'open', 'watch', done
  return

gulp.task 'serveForE2E', (done) ->
  runSequence 'build', 'connect', done
  return

gulp.task 'watch', () ->
  gulp.watch config.watch, ['reconnect']
  return

gulp.task 'postbuild-cleanup', (done) ->
  clean ["#{config.buildJs}bundle.js", "#{config.buildJs}rev-manifest.json"], done

gulp.task 'clean-tmp', (done) ->
  clean config.tmp, done
  return

gulp.task 'clean-build', (done) ->
  clean config.build, done
  return

gulp.task 'help', () ->
  $.taskListing()
  return

# todo list

# dist for building deployment package and running e2e tests

# convert less to css and minify

# run unit tests using Karma
gulp.task 'build-for-unit-tests', (done) ->
  log('Running build for unit tests')
  runSequence(['coffeelint', 'clean-tmp', 'clean-build'], 'wiredep', 'compilecoffee', 'templatecache', done)
  log('Done running build for unit tests')
  return

gulp.task 'unittests', (done) ->
  runKarma config.karmaConfigFile, {
    autoWatch: false,
    singleRun: true
  }, done


gulp.task 'unittests-watch', (done) ->
  runKarma config.karmaConfigFile, {
    autoWatch: true,
    singleRun: false
  }, done

# run e2e
gulp.task 'e2e', (done) ->
  runSequence 'serveForE2E', 'protractor', 'stopConnect', done
  return

gulp.task 'protractor', () ->
  protractor = require('gulp-protractor').protractor

  gulp.src(config.e2eTestsGlob)
  .pipe(protractor({
      configFile: config.protractorConfigFile,
      args: ['--baseUrl', 'http://localhost:9000']
    }))
  .pipe($.exit())
  .on('error', (e) -> throw e)

########################################################

clean = (path, done) ->
  log "Cleaning: #{$.util.colors.green(path)}"
  del path, done
  return

log = (msg) ->
  if typeof(msg) is 'object'
    for item in msg
      if msg.hasOwnProperty item
        $.util.log $.util.colors.blue(msg[item])
  else
    $.util.log($.util.colors.blue(msg));

  return

runKarma = (configFilePath, options, cb) ->
  configFilePath = path.resolve configFilePath
  server = karma.server
  config = karmaParseConfig configFilePath, {}

  Object.keys(options).forEach((key) ->
    config[key] = options[key]
  )

  server.start(config, (exitCode) ->
    log("Karma has exited with #{exitCode}")
    cb()
    process.exit(exitCode)
  )